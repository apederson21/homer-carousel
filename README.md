This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Setup

This project was build using Node `v14.15.4 (npm v6.14.10)`.

If you use NVM run `nvm use`. Otherwise, ensure you have and are running the version above of NodeJS.

## Available Scripts

| script                | purpose                                                 | notes                                          |
| --------------------- | ------------------------------------------------------- | ---------------------------------------------- |
| `npm start`           | Runs the app in the development mode                    | [http://localhost:3000](http://localhost:3000) |
| `npm test`            | Launches the test runner in the interactive watch mode. |                                                |
| `npm run test:update` | Launches the test runner in the interactive watch mode. | Updates snapshots                              |

## More Info

Please see the project [Wiki](https://gitlab.com/apederson21/homer-carousel/-/wikis/home) for more information.

## Demo

<img src="./demo/application.gif" />
