import React from 'react';
import './ImageViewer.css';
import Image from '../Image/Image';

/**
 * A <div> with 500x500 <Image> inside.
 *
 * @param {object} props
 * @param {string} caption Image caption
 * @param {string} source Image source
 *
 * @returns {HTMLDivElement}
 */
function ImageViewer({ caption, source }) {
    return (
        <div className="image-viewer">
            <Image caption={caption} height={500} width={500} source={source} />
        </div>
    );
}

export default ImageViewer;
