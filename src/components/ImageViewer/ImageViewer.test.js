import { render } from '@testing-library/react';
import React from 'react';
import ImageViewer from './ImageViewer';

describe('components: ImageViewer', () => {
    afterEach(() => jest.clearAllMocks());

    test('renders correctly with all props', () => {
        const props = {
            caption: 'img caption',
            source: 'img source'
        };
        const component = render(<ImageViewer {...props} />);
        expect(component).toMatchSnapshot();
    });

    test('renders correctly when missing all props', () => {
        const props = {};
        const component = render(<ImageViewer {...props} />);
        expect(component).toMatchSnapshot();
    });
});
