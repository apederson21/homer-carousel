import React, { useState } from 'react';
import './ImageSelector.css';
import Heading from '../Heading/Heading';
import Image from '../Image/Image';
import Button from '../Button/Button';
import { noOp } from '../../utils/utils';

/**
 * A <div> element with heading, images, and button.
 *
 * @param {object} props
 * @param {array} [props.images=[]] Array of image objects
 * @param {function} [props.onSetImages=noOp] Handler for Adding images
 *
 * @returns {HTMLDivElement}
 */
function ImageSelector({ images = [], onSetImages = noOp }) {
    const [selectedImages, setSelectedImages] = useState({});

    function submitSelectedImagesAndClear() {
        onSetImages(selectedImages);
        setSelectedImages({});
    }

    /**
     * Handle image being clicked.
     *
     * If the image ID is not currently
     * in the `selectedImages` object
     * it will be added.
     *
     * If it is currently present,
     * it will be removed.
     *
     * @param {number} id
     */
    function handleImageClick(id) {
        const stagingObject = { ...selectedImages };
        stagingObject[id] ? delete stagingObject[id] : (stagingObject[id] = true);
        setSelectedImages(stagingObject);
    }

    const heading = 'Please select images for your carousel';

    const imagesArray = images.map(({ id, imageCaption, imageName }) => (
        <Image
            key={id}
            caption={imageCaption}
            classNames={selectedImages[id] ? 'selected' : ''}
            dataTestId={`image-${id}`}
            onClick={() => handleImageClick(id)}
            source={`${process.env.PUBLIC_URL}/assets/${imageName}`}
        />
    ));

    const buttonDisabledValue = Object.keys(selectedImages).length == 0;

    return (
        <div className="image-selector">
            <Heading text={heading} />
            <div className="images">{imagesArray}</div>
            <div className="button-wrapper">
                <Button
                    dataTestId="add-button"
                    disabled={buttonDisabledValue}
                    label="Add to Carousel"
                    onClick={submitSelectedImagesAndClear}
                />
            </div>
        </div>
    );
}

export default ImageSelector;
