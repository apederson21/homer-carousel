import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import ImageSelector from './ImageSelector';

describe('components: ImageSelector', () => {
    afterEach(() => jest.clearAllMocks());

    const props = {
        images: [
            {
                id: 0,
                imageCaption: 'caption 1',
                imageName: 'image1.jpg'
            },
            {
                id: 1,
                imageCaption: 'caption 2',
                imageName: 'image2.jpg'
            },
            {
                id: 2,
                imageCaption: 'caption 3',
                imageName: 'image3.jpg'
            }
        ],
        onSetImages: jest.fn()
    };

    test('renders correctly with all props', () => {
        const component = render(<ImageSelector {...props} />);
        expect(component).toMatchSnapshot();
        expect(props.onSetImages).toHaveBeenCalledTimes(0);
    });

    test('renders correctly with no props', () => {
        const component = render(<ImageSelector />);
        expect(component).toMatchSnapshot();
        expect(props.onSetImages).toHaveBeenCalledTimes(0);
    });

    test('updates internal state and submits selected images', () => {
        render(<ImageSelector {...props} />);

        // select first and last images
        fireEvent.click(screen.getByTestId('image-0'));
        fireEvent.click(screen.getByTestId('image-2'));

        // leverage add button
        fireEvent.click(screen.getByTestId('add-button'));

        // assert the correct images are being passed
        expect(props.onSetImages).toHaveBeenCalledTimes(1);
        expect(props.onSetImages).toHaveBeenCalledWith({
            0: true,
            2: true
        });
    });
});
