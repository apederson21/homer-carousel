import React from 'react';
import './Heading.css';

/**
 * An <h2> component.
 *
 * @param {object} props
 * @param {string} props.text
 *
 * @returns {HTMLHeadingElement}
 */
function Heading({ text }) {
    return <h2>{text}</h2>;
}

export default Heading;
