import { render } from '@testing-library/react';
import React from 'react';
import Heading from './Heading';

describe('components: Heading', () => {
    afterEach(() => jest.clearAllMocks());

    test('renders correctly with all props', () => {
        const props = {
            text: 'my heading'
        };
        const component = render(<Heading {...props} />);
        expect(component).toMatchSnapshot();
    });

    test('renders correctly when missing all props', () => {
        const props = {};
        const component = render(<Heading {...props} />);
        expect(component).toMatchSnapshot();
    });
});
