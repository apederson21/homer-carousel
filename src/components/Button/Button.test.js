import { render } from '@testing-library/react';
import React from 'react';
import Button from './Button';

describe('components: Button', () => {
    afterEach(() => jest.clearAllMocks());

    test('renders correctly with all props', () => {
        const clickHandler = jest.fn();
        const buttonLabel = 'my label';
        const props = {
            classNames: 'class1 class2',
            dataTestid: 'test',
            disabled: true,
            label: buttonLabel,
            onClick: clickHandler
        };
        const component = render(<Button {...props} />);
        expect(component).toMatchSnapshot();
    });

    test('renders correctly when missing all props', () => {
        const props = {};
        const component = render(<Button {...props} />);
        expect(component).toMatchSnapshot();
    });
});
