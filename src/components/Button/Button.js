import React from 'react';
import { noOp } from '../../utils/utils';

/**
 * A <button> component
 *
 * @param {object} props
 * @param {string} [props.classNames=''] Button class names
 * @param {string} [props.dataTestId=''] Optional testid
 * @param {boolean} [props.disabled=false] Button disabled
 * @param {string} [props.label=''] Button label
 * @param {function} [props.onClick=()=>{}] Button onClick handler
 *
 * @returns {HTMLButtonElement}
 */
function Button({ classNames = '', dataTestId = '', disabled = false, label = '', onClick = noOp }) {
    return (
        <button
            className={classNames}
            data-testid={dataTestId}
            aria-label={label}
            disabled={disabled}
            onClick={onClick}
        >
            {label}
        </button>
    );
}

export default Button;
