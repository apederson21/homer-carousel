/**
 * Determine how many Carousel
 * moves right based on total
 * number of images and the
 * current viewCount.
 *
 * @param {array} images
 * @param {string} viewCount
 *
 * @returns {number}
 */
export function getMovesRight(images, viewCount) {
    const parsedViewCount = parseInt(viewCount);
    const viewCountNoRemainder = images.length % parsedViewCount === 0;

    const imagesPerPage = images.length / parsedViewCount;
    if (viewCountNoRemainder) {
        return imagesPerPage - 1;
    }

    return parseInt(imagesPerPage);
}

/**
 * Move the Carousel to the beginning
 * by setting the "left" CSS value
 * to "0px".
 *
 * @param {RefObject} carouselRef
 */
export function moveCarouselToBeginning(carouselRef) {
    carouselRef.current.style.left = '0px';
}
