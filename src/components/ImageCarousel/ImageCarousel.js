import React, { useEffect, useRef, useState } from 'react';
import './ImageCarousel.css';
import Image from '../Image/Image';
import Select from '../Select/Select';
import Toggle from '../Toggle/Toggle';
import Button from '../Button/Button';
import { noOp } from '../../utils/utils';
import { getMovesRight, moveCarouselToBeginning } from './ImageCarouselHelpers';

/**
 * Helper variable to determine
 * when to trigger re-calculations
 * of Carousel moves left.
 * This can happen in a few spots
 * like:
 *  - changing the viewCount from 2 to 5
 *  - adding images
 *  - deleting images
 */
let originalImageLength = 0;

/**
 * A <div> element with heading, images, and buttons.
 *
 * @param {object} props
 * @param {array} [props.images=[]] Array of image objects
 * @param {function} [props.onDeleteImages=noOp] Handler for adding images
 * @param {function} [props.onEditMode=noOp] Handler for entering "Edit" mode
 * @param {function} [props.onImageViewer=noOp] Handler for selecting an image for the viewer
 *
 * @returns {HTMLDivElement}
 */
function ImageCarousel({ images = [], onDeleteImages = noOp, onEditMode = noOp, onImageViewer = noOp }) {
    const [selectedImages, setSelectedImages] = useState({});
    const [viewCount, setViewCount] = useState('2');
    const [mode, setMode] = useState('view');
    const [movesLeft, setMovesLeft] = useState(0);
    const [movesRight, setMovesRight] = useState(getMovesRight(images, viewCount));
    const carouselRef = useRef();
    const isView = mode === 'view';
    const isEdit = mode === 'edit';
    const viewOptions = [
        {
            label: 'Two',
            value: '2'
        },
        {
            label: 'Five',
            value: '5'
        }
    ];

    /**
     * Handler for changing ViewCount betwen 2 and 5.
     *
     * @param {Event}
     */
    function handleViewCountChange({ target }) {
        const newState = viewOptions[target.selectedIndex].value;
        originalImageLength = 0;
        setViewCount(newState);
    }

    /**
     * Handler for toggling between
     * "Edit" and "View" modes.
     *
     * @param {boolean} isToggledOn
     */
    function handleToggleChange(isToggledOn) {
        setSelectedImages({});
        if (isToggledOn) {
            onEditMode();
        }
        setMode(isToggledOn ? 'edit' : 'view');
    }

    /**
     * Handle image being clicked.
     *
     * If the image ID is not currently
     * in the `selectedImages` object
     * it will be added.
     *
     * If it is currently present,
     * it will be removed.
     *a
     * @param {number} id
     */
    function handleImageClick(id) {
        const stagingObject = { ...selectedImages };
        stagingObject[id] ? delete stagingObject[id] : (stagingObject[id] = true);
        setSelectedImages(stagingObject);
    }

    /**
     * Create the array of carousel images.
     */
    const imagesArray = images.map(({ id, imageCaption, imageName }) => (
        <Image
            key={id}
            caption={imageCaption}
            classNames={selectedImages[id] ? 'selected' : ''}
            dataTestId={`image-${id}`}
            hideCaption={true}
            onClick={isEdit ? () => handleImageClick(id) : () => onImageViewer(id)}
            source={`${process.env.PUBLIC_URL}/assets/${imageName}`}
        />
    ));

    /**
     * Handler for deleting an image
     * from the Carousel.
     */
    function handleDeleteImage() {
        if (Object.keys(selectedImages).length === 0) {
            return;
        }

        // tell the App images were deleted from the carousel
        onDeleteImages(selectedImages);

        // cleanup and Carousel reset
        setSelectedImages({});
        moveCarouselToBeginning(carouselRef);
    }

    /**
     * Handle cycling backwards in
     * the Carousel.
     */
    function handlePrev() {
        const newMovesLeft = movesLeft - 1;
        setMovesLeft(newMovesLeft);
        setMovesRight(movesRight + 1);
        moveCarousel('left', newMovesLeft === 0);
    }

    /**
     * Handle cycling forwards in
     * the Carousel.
     */
    function handleNext() {
        const newMovesRight = movesRight - 1;
        setMovesLeft(movesLeft + 1);
        setMovesRight(newMovesRight);
        moveCarousel('right', newMovesRight === 0);
    }

    /**
     * When we receive new images or
     * remove some, reset the carousel.
     */
    useEffect(() => {
        if (images.length !== originalImageLength) {
            originalImageLength = images.length;
            setMovesLeft(0);
            setMovesRight(getMovesRight(images, viewCount));
            moveCarouselToBeginning(carouselRef);
        }
    });

    /**
     * Move the Carousel by altering the
     * CSS "left" style value.
     *
     * If we are not at a boundary, we check
     * the total number of images to
     * determine how much to slide.
     *
     * When there are only a couples images
     * remanining to the left or right, the
     * Carousel will only slide enough to
     * reveal the remaining images.
     *
     * @param {string} direction "left" or "right"
     * @param {boolean} isBoundary Whether we are at the end of moves or not
     */
    function moveCarousel(direction, isBoundary) {
        const carouselWidth = 500; // default full carousel slide
        const movementByImageCount = {
            2: 250,
            5: 100
        };
        const parsedViewcount = parseInt(viewCount);
        const imagesRemaining = images.length % parsedViewcount;

        /**
         * When we hit the boundary, check for
         * partial images remaining so we don't
         * end up sliding to reveal empty
         * images spaces.
         */
        let partialCycle;
        if (isBoundary && imagesRemaining !== 0) {
            partialCycle = imagesRemaining * movementByImageCount[viewCount];
        }

        /**
         * Grab the current left style numeric value,
         * then do the appropriate math against it
         * for sliding right or left.
         */
        const regexLeft = /[0-9|-]/g;
        const regexRight = /[0-9]/g;
        const currentLeftValueMatches = carouselRef.current.style.left.match(
            direction === 'left' ? regexLeft : regexRight
        ) || [0];
        const currentLeftValue = parseInt(currentLeftValueMatches.join(''));

        // Set new left value
        if (direction === 'left') {
            carouselRef.current.style.left = `${currentLeftValue + (partialCycle || carouselWidth)}px`;
        } else if (direction === 'right') {
            carouselRef.current.style.left = `-${currentLeftValue + (partialCycle || carouselWidth)}px`;
        }
    }

    const isMoveLeftDisabled = movesLeft === 0;
    const isMoveRightDisabled = movesRight === 0;

    return (
        <div className={`image-carousel carousel-${viewCount}`}>
            <div className="options-wrapper">
                <Select
                    dataTestId="view-count"
                    label="Visible image count:"
                    name="viewCount"
                    options={viewOptions}
                    onChange={handleViewCountChange}
                ></Select>
                <Button
                    classNames="delete-button"
                    dataTestId="delete-button"
                    disabled={isView}
                    label="Delete Selected"
                    onClick={handleDeleteImage}
                ></Button>
                <Toggle onChange={handleToggleChange} offText="View" onText="Edit" />
            </div>
            <div className="obscure-left"></div>
            <div className={`images display-${viewCount}`} ref={carouselRef}>
                {imagesArray}
            </div>
            <div className="obscure-right"></div>
            <Button
                dataTestId={`move-left${isMoveLeftDisabled ? '-disabled' : ''}`}
                disabled={isMoveLeftDisabled}
                classNames="previous"
                onClick={handlePrev}
            />
            <Button
                dataTestId={`move-right${isMoveRightDisabled ? '-disabled' : ''}`}
                disabled={isMoveRightDisabled}
                classNames="next"
                onClick={handleNext}
            />
        </div>
    );
}

export default ImageCarousel;
