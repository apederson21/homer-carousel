import { getMovesRight, moveCarouselToBeginning } from './ImageCarouselHelpers';

describe('components: ImageCarouselHelpers', () => {
    afterEach(() => jest.clearAllMocks());

    describe('getMovesRight', () => {
        test('returns 0 when image length and viewCount are the same', () => {
            expect(getMovesRight([1], 1)).toEqual(0);
        });
        test('returns 0 when image length is less than viewCount', () => {
            expect(getMovesRight([1], 5)).toEqual(0);
        });
        test('retuns the difference when image length is greater than viewCount with remainder', () => {
            expect(getMovesRight([1, 2, 3, 4, 5], 2)).toEqual(2);
        });
        test('retuns the difference minus 1 when image length is greater than viewCount with no remainder', () => {
            expect(getMovesRight([1, 2, 3, 4, 5, 6], 2)).toEqual(2);
        });
    });

    describe('moveCarouselToBeginning', () => {
        test('sets the style left to "0px"', () => {
            const input = {
                current: {
                    style: {
                        left: '100px'
                    }
                }
            };
            moveCarouselToBeginning(input);
            expect(input.current.style.left).toEqual('0px');
        });
    });
});
