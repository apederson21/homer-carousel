import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import ImageCarousel from './ImageCarousel';

describe('components: ImageSelector', () => {
    afterEach(() => jest.clearAllMocks());

    const props = {
        images: [
            {
                id: 0,
                imageCaption: 'caption 1',
                imageName: 'image1.jpg'
            },
            {
                id: 1,
                imageCaption: 'caption 2',
                imageName: 'image2.jpg'
            },
            {
                id: 2,
                imageCaption: 'caption 3',
                imageName: 'image3.jpg'
            }
        ],
        onDeleteImages: jest.fn(),
        onEditMode: jest.fn(),
        onImageViewer: jest.fn()
    };

    test('renders correctly with all props', () => {
        const component = render(<ImageCarousel {...props} />);
        expect(component).toMatchSnapshot();
        expect(props.onDeleteImages).toHaveBeenCalledTimes(0);
        expect(props.onEditMode).toHaveBeenCalledTimes(0);
        expect(props.onImageViewer).toHaveBeenCalledTimes(0);
    });

    test('renders correctly with no props', () => {
        const component = render(<ImageCarousel />);
        expect(component).toMatchSnapshot();
        expect(props.onDeleteImages).toHaveBeenCalledTimes(0);
        expect(props.onEditMode).toHaveBeenCalledTimes(0);
        expect(props.onImageViewer).toHaveBeenCalledTimes(0);
    });

    test('can successfully change the viewCount', () => {
        const component = render(<ImageCarousel {...props} />);

        fireEvent.change(screen.getByTestId('view-count'), {
            target: { value: '5' }
        });

        expect(component).toMatchSnapshot();
        expect(props.onDeleteImages).toHaveBeenCalledTimes(0);
        expect(props.onEditMode).toHaveBeenCalledTimes(0);
        expect(props.onImageViewer).toHaveBeenCalledTimes(0);
    });

    test('successfully triggers the image viewer', () => {
        render(<ImageCarousel {...props} />);

        fireEvent.click(screen.getByTestId('image-1'));

        expect(props.onDeleteImages).toHaveBeenCalledTimes(0);
        expect(props.onEditMode).toHaveBeenCalledTimes(0);
        expect(props.onImageViewer).toHaveBeenCalledTimes(1);
        expect(props.onImageViewer).toHaveBeenCalledWith(1);
    });

    test('can cycle forwards and backwards when images extend beyond carousel', () => {
        render(<ImageCarousel {...props} />);

        expect(screen.getByTestId('move-left-disabled')).toBeInTheDocument();
        expect(screen.getByTestId('move-right')).toBeInTheDocument();

        // move right
        fireEvent.click(screen.getByTestId('move-right'));
        expect(screen.getByTestId('move-left')).toBeInTheDocument();
        expect(screen.getByTestId('move-right-disabled')).toBeInTheDocument();

        // move left
        fireEvent.click(screen.getByTestId('move-left'));
        expect(screen.getByTestId('move-left-disabled')).toBeInTheDocument();
        expect(screen.getByTestId('move-right')).toBeInTheDocument();

        expect(props.onDeleteImages).toHaveBeenCalledTimes(0);
        expect(props.onEditMode).toHaveBeenCalledTimes(0);
        expect(props.onImageViewer).toHaveBeenCalledTimes(0);
    });

    test('has delete button disabled until images are selected', () => {
        render(<ImageCarousel {...props} />);

        // attempt delete (button disabled)
        fireEvent.click(screen.getByTestId('delete-button'));
        expect(props.onDeleteImages).toHaveBeenCalledTimes(0);

        // turn on edit mode
        fireEvent.click(screen.getByTestId('toggle-check'));

        // attempt delete (button disabled)
        fireEvent.click(screen.getByTestId('delete-button'));
        expect(props.onDeleteImages).toHaveBeenCalledTimes(0);

        // select image 2
        fireEvent.click(screen.getByTestId('image-1'));

        // click delete button again
        fireEvent.click(screen.getByTestId('delete-button'));
        expect(props.onDeleteImages).toHaveBeenCalledTimes(1);
        expect(props.onDeleteImages).toHaveBeenCalledWith({ 1: true });

        expect(props.onEditMode).toHaveBeenCalledTimes(1);
        expect(props.onImageViewer).toHaveBeenCalledTimes(0);
    });
});
