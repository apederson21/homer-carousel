import React, { useState } from 'react';
import ImageSelector from '../ImageSelector/ImageSelector';
import ImageCarousel from '../ImageCarousel/ImageCarousel';
import { carouselImages } from '../../data/carouselImages.json';
import { alphabetizeImagesByCaption, addUniqueIdsToImages } from '../../utils/utils';
import ImageViewer from '../ImageViewer/ImageViewer';

/**
 * Alphabetize and append IDs to our data set.
 */
const alphabetizedImages = alphabetizeImagesByCaption(carouselImages);
const imagesWithIds = addUniqueIdsToImages(alphabetizedImages);

function App() {
    const [imageStateArray, setImageStateArray] = useState(imagesWithIds);
    const [imageViewerCurrent, setImageViewerCurrent] = useState({});

    /**
     * Handle images being added to the Carousel
     * from the Image Selector.
     *
     * @param {object} selectedImages
     */
    function handleSetCarouselImages(selectedImages) {
        // basic error scenario handling
        if (Object.keys(selectedImages).length == 0) {
            return;
        }

        const selectedImageIds = Object.keys(selectedImages);
        const stagingArray = [...imageStateArray].map(imageObject => {
            if (selectedImageIds.includes(String(imageObject.id))) {
                imageObject.carousel = true;
            }
            return imageObject;
        });

        setImageStateArray(stagingArray);
    }

    /**
     * Handle an image being sent to the
     * ImageViewer from the Carousel.
     *
     * @param {number} imageId
     */
    function handleOnImageViewer(imageId) {
        if (imageViewerCurrent && imageViewerCurrent.id === imageId) {
            return setImageViewerCurrent({});
        }

        const updatedImageViewerCurrent = imageStateArray.find(imageObject => imageObject.id === imageId);
        if (updatedImageViewerCurrent) {
            setImageViewerCurrent(updatedImageViewerCurrent);
        }
    }

    /**
     * When the Carousel is in "Edit"
     * mode, we should hide the viewer.
     */
    function handleOnEditMode() {
        setImageViewerCurrent({});
    }

    /**
     * Handle the removal of images from the
     * carousel by sending them back to the
     * selector.
     *
     * @param {object} selectedImages
     */
    function handleDeleteCarouselImages(selectedImages) {
        // basic error scenario handling
        if (Object.keys(selectedImages).length == 0) {
            return;
        }

        const deletedImageIds = Object.keys(selectedImages);
        const stagingArray = [...imageStateArray].map(imageObject => {
            if (deletedImageIds.includes(String(imageObject.id))) {
                imageObject.carousel = false;
            }
            return imageObject;
        });

        setImageStateArray(stagingArray);
    }

    const currentCarouselImages = imageStateArray.filter(imageObject => imageObject.carousel);

    return (
        <React.Fragment>
            <ImageSelector
                images={imageStateArray.filter(imageObject => !imageObject.carousel)}
                onSetImages={handleSetCarouselImages}
            />
            {currentCarouselImages.length > 0 && (
                <ImageCarousel
                    images={currentCarouselImages}
                    onDeleteImages={handleDeleteCarouselImages}
                    onImageViewer={handleOnImageViewer}
                    onEditMode={handleOnEditMode}
                />
            )}
            {imageViewerCurrent.imageCaption && imageViewerCurrent.imageName ? (
                <ImageViewer
                    caption={imageViewerCurrent.imageCaption}
                    source={`${process.env.PUBLIC_URL}/assets/${imageViewerCurrent.imageName}`}
                />
            ) : null}
        </React.Fragment>
    );
}

export default App;
