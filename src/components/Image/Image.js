import React from 'react';
import './Image.css';
import { noOp } from '../../utils/utils';

/**
 * A <span> element with image and caption.
 *
 * @param {object} props
 * @param {string} [props.caption=''] Image caption
 * @param {string} [props.classNames=''] Image class names
 * @param {string} [props.dataTestId=''] Optional testid
 * @param {number} [props.height=100] Image height
 * @param {boolean} [props.hideCaption=false] Whether to hid caption text or not
 * @param {function} [props.onClick=noOp] Image onClick handler
 * @param {string} props.source Image source
 * @param {number} [props.width=100] Image width
 *
 * @returns {HTMLSpanElement}
 */
function Image({
    caption = '',
    classNames = '',
    dataTestId = '',
    height = 100,
    hideCaption = false,
    onClick = noOp,
    source,
    width = 100
}) {
    const className = `image ${classNames}`.trim();
    return (
        <span className={className}>
            <img
                aria-label={caption}
                data-testid={dataTestId}
                onClick={onClick}
                src={source}
                height={height}
                width={width}
            />
            {!hideCaption && <span>{caption}</span>}
        </span>
    );
}

export default Image;
