import { render } from '@testing-library/react';
import React from 'react';
import Image from './Image';

describe('components: Image', () => {
    afterEach(() => jest.clearAllMocks());

    test('renders correctly with all props', () => {
        const props = {
            caption: 'my caption',
            classNames: 'class1 class 2',
            dataTestid: 'test',
            height: 250,
            hideCaption: true,
            onClick: jest.fn(),
            source: 'learnwithhomer.com',
            width: 300
        };
        const component = render(<Image {...props} />);
        expect(component).toMatchSnapshot();
        expect(props.onClick).toHaveBeenCalledTimes(0);
    });

    test('renders correctly when missing all props', () => {
        const props = {};
        const component = render(<Image {...props} />);
        expect(component).toMatchSnapshot();
    });
});
