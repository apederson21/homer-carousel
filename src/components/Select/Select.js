import React from 'react';
import { noOp } from '../../utils/utils';

/**
 * A <label> with select dropdown inside.
 *
 * @param {object} props
 * @param {string} [props.dataTestId='']
 * @param {string} [props.label=''] Label for Select
 * @param {string} [props.name=''] Name for Select
 * @param {function} [props.onChange=noOp] Change handler for Select
 * @param {array} [props.options=[]] Select options [{ label, value }]
 *
 * @returns {HTMLLabelElement}
 */
function Select({ dataTestId = '', label = '', name = '', onChange = noOp, options = [] }) {
    const optionElements = options.map(({ label, value }, i) => (
        <option key={i} value={value}>
            {label}
        </option>
    ));
    return (
        <label htmlFor={name}>
            {label}&nbsp;
            <select data-testid={dataTestId} name={name} onChange={onChange}>
                {optionElements}
            </select>
        </label>
    );
}

export default Select;
