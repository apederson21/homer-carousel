import { render } from '@testing-library/react';
import React from 'react';
import Select from './Select';

describe('components: Select', () => {
    afterEach(() => jest.clearAllMocks());

    test('renders correctly with all props', () => {
        const props = {
            dataTestId: 'test',
            label: 'my label',
            name: 'my name',
            onChange: jest.fn(),
            options: [
                {
                    label: 'Letter A',
                    value: 'a'
                },
                {
                    label: 'Letter B',
                    value: 'b'
                },
                {
                    label: 'Letter C',
                    value: 'c'
                }
            ]
        };
        const component = render(<Select {...props} />);
        expect(component).toMatchSnapshot();
    });

    test('renders correctly when missing all props', () => {
        const props = {};
        const component = render(<Select {...props} />);
        expect(component).toMatchSnapshot();
    });
});
