import React, { useState } from 'react';
import './Toggle.css';
import { noOp } from '../../utils/utils';

/**
 * A <lable> component with toggle switch.
 *
 * @param {object} props
 * @param {function} [props.onChange=noOp] Handle for toggle change; Returns checked state (checked = on)
 * @param {string} [props.offText="Off"] Text displayed when toggle is off
 * @param {string} [props.onText="On"] Text displayed when toggle is on
 *
 * @returns {HTMLLabelElement}
 */
function Toggle({ onChange = noOp, offText = 'Off', onText = 'On' }) {
    const [toggleState, updateToggleState] = useState({ toggled: false });

    function handleToggleChange({ target }) {
        updateToggleState({ toggled: target.checked });
        onChange(target.checked);
    }

    return (
        <label className="toggle">
            <span className="label">{toggleState.toggled ? onText : offText}</span>
            <span className="toggle-checkbox">
                <input data-testid="toggle-check" type="checkbox" onChange={handleToggleChange} />
                <span className="slider"></span>
            </span>
        </label>
    );
}

export default Toggle;
