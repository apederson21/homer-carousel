import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import Toggle from './Toggle';

describe('components: Toggle', () => {
    afterEach(() => jest.clearAllMocks());

    test('renders correctly with all props', () => {
        const props = {
            onChange: jest.fn(),
            offText: 'I am off',
            onText: 'I am on'
        };
        const component = render(<Toggle {...props} />);
        expect(component).toMatchSnapshot();

        // exercise onChange
        const checkboxElement = screen.getByTestId('toggle-check');
        fireEvent.click(checkboxElement);
        expect(props.onChange).toHaveBeenCalledWith(true);

        fireEvent.click(checkboxElement);
        expect(props.onChange).toHaveBeenCalledWith(false);
    });

    test('renders correctly when missing all props', () => {
        const props = {};
        const component = render(<Toggle {...props} />);
        expect(component).toMatchSnapshot();
    });
});
