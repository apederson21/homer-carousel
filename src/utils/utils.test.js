import { alphabetizeImagesByCaption, addUniqueIdsToImages, noOp } from './utils';

describe('utils', () => {
    afterEach(() => jest.clearAllMocks());

    test('noOp is a function', () => {
        expect(typeof noOp).toEqual('function');
    });

    describe('alphabetizeImagesByCaption', () => {
        test('array of image objects are ordered', () => {
            const input = [
                {
                    imageName: 'zoo.jpg',
                    imageCaption: 'ZoO'
                },
                {
                    imageName: 'bagel_with_cc.jpg',
                    imageCaption: 'Bagel With Cream Cheese'
                },
                {
                    imageName: 'taco.jpg',
                    imageCaption: 'Taco'
                },
                {
                    imageName: 'zoo2.jpg',
                    imageCaption: 'ZoO2'
                },
                {
                    imageName: 'taco2.jpg',
                    imageCaption: 'Taco'
                },
                {
                    imageName: 'bagel.jpg',
                    imageCaption: 'Bagel'
                }
            ];
            const expectedOrder = [
                {
                    imageName: 'bagel.jpg',
                    imageCaption: 'Bagel'
                },
                {
                    imageName: 'bagel_with_cc.jpg',
                    imageCaption: 'Bagel With Cream Cheese'
                },
                {
                    imageName: 'taco.jpg',
                    imageCaption: 'Taco'
                },
                {
                    imageName: 'taco2.jpg',
                    imageCaption: 'Taco'
                },
                {
                    imageName: 'zoo.jpg',
                    imageCaption: 'ZoO'
                },
                {
                    imageName: 'zoo2.jpg',
                    imageCaption: 'ZoO2'
                }
            ];

            const result = alphabetizeImagesByCaption(input);

            // assert input was not mutated
            expect(input[0].imageCaption).toEqual('ZoO');
            expect(input[1].imageCaption).toEqual('Bagel With Cream Cheese');
            expect(input[2].imageCaption).toEqual('Taco');
            expect(input[3].imageCaption).toEqual('ZoO2');
            expect(input[4].imageCaption).toEqual('Taco');
            expect(input[5].imageCaption).toEqual('Bagel');

            // assert correct order
            result.forEach((result, i) => {
                expect(result.imageCaption).toEqual(expectedOrder[i].imageCaption);
            });
        });
    });
    describe('addUniqueIdsToImages', () => {
        test('array index is added to image objects as `id`', () => {
            const input = [
                {
                    image: '1'
                },
                {
                    image: '2'
                }
            ];

            const output = addUniqueIdsToImages(input);

            // assert input was not mutated
            expect(input[0].id).toBeUndefined();
            expect(input[1].id).toBeUndefined();

            // assert id is present and correct
            expect(output[0].id).toEqual(0);
            expect(output[1].id).toEqual(1);
        });
    });
});
