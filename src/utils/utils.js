export const noOp = () => {};

/**
 * Alphabetize an array of image objects by the
 * `imageCaption` value.
 *
 * @param {array} images [{ imageName, imageCaption, etc.. }]
 * @returns {array}
 */
export function alphabetizeImagesByCaption(images) {
    // clone array
    const clonedImagesArray = Array.from(images);

    // sort array
    return clonedImagesArray.sort(({ imageCaption: aCaption }, { imageCaption: bCaption }) => {
        const nameA = aCaption.trim().toUpperCase();
        const nameB = bCaption.trim().toUpperCase();

        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }

        // captions are equal
        return 0;
    });
}

/**
 * Add unique IDs to each image object in the array.
 *
 * @param {array} images [{ imageName, imageCaption, etc.. }]
 * @returns {array}
 */
export function addUniqueIdsToImages(images) {
    return images.map((image, i) => ({ ...image, id: i }));
}
